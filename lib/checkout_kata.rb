require "checkout_kata/version"
require "checkout_kata/v1/checkout"

require "checkout_kata/v2/rules"
require "checkout_kata/v2/checkout"

require "checkout_kata/v3/rules"
require "checkout_kata/v3/cart"
require "checkout_kata/v3/checkout"

require "checkout_kata/v4/rules"
require "checkout_kata/v4/cart"
require "checkout_kata/v4/checkout"

module CheckoutKata
end
