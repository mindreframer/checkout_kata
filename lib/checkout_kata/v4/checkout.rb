module CheckoutKata
  module V4
    class CheckOut
      attr_accessor :rules
      def initialize(rules)
        @rules = rules
      end

      def scan(item)
        cart.add(item)
      end

      def total
        result = 0
        items  = cart.copy_items_map
        rules.each do |rule|
          while rule.applicable?(items)
            amount, items = rule.apply(items)
            result += amount
          end
        end
        return result
      end

      private

      def cart
        @cart ||= CheckoutKata::V4::Cart.new
      end
    end
  end
end
