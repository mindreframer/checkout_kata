# V4 (introduce bundle pricing rules) solution for the checkout kata (http://codekata.com/kata/kata09-back-to-the-checkout/)


We implemented `ItemBundleRule` that allows some particular combination of items and amounts for promotions.

Pros:

  - just prooving that it works

Cons:

  - no new ones
