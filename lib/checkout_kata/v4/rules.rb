module CheckoutKata
  module V4
    class AbstractRule
      # applies this rule to the cart state
      #
      # @param current_cart_state [Map [Item -> amount] current_cart_state
      #
      # @return [Integer]              calculated amount for this rule
      # @return [Map [Item -> amount]] new current cart state
      #
      def apply(current_cart_state)
        raise "Implement in subclass"
      end

      # checks whether it is neccessary/possible to apply this rule
      #
      # @param current_cart_state [Map [Item -> amount] current_cart_state
      #
      # @return [boolean] should this rule be applied
      #
      def applicable?(current_cart_state)
        raise "Implement in subclass"
      end
    end

    # ItemAmountRule implements promotion rules based on specific amounts
    # of a single item in the shopping cart
    #
    class ItemAmountRule < CheckoutKata::V4::AbstractRule
      attr_accessor :name, :amount, :price
      def initialize(name:, amount:, price:)
        @name, @amount, @price = name, amount, price
      end

      def apply(current_cart_state)
        return 0, current_cart_state if not applicable?(current_cart_state)
        copy = current_cart_state.dup
        copy[name] -= amount
        return price, copy
      end

      def applicable?(current_cart_state)
        current_cart_state[name] && current_cart_state[name] >= amount
      end
    end

    # ItemBundleRule implements promotion rules based on specific amounts
    # of multilpe items in the shopping cart
    #
    # Example:
    #   > ItemBundleRule.new(name_amount_pairs: [["A", 2], ["B", 1]], price: 12)
    #
    class ItemBundleRule < CheckoutKata::V4::AbstractRule
      attr_accessor :name_amount_pairs, :price
      def initialize(name_amount_pairs:, price:)
        @name_amount_pairs, @price = name_amount_pairs, price
      end

      def apply(current_cart_state)
        return 0, current_cart_state if not applicable?(current_cart_state)

        copy = current_cart_state.dup
        name_amount_pairs.map do |pair|
          name   = pair[0]
          amount = pair[1]
          copy[name] -= amount
        end

        return price, copy
      end

      def applicable?(current_cart_state)
        name_amount_pairs.map do |pair|
          name   = pair[0]
          amount = pair[1]
          current_cart_state[name] && current_cart_state[name] >= amount
        end.uniq == [true]
      end
    end
  end
end
