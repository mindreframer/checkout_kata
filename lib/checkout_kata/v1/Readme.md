# V1 (MVP) solution for the checkout kata (http://codekata.com/kata/kata09-back-to-the-checkout/)

For the initial solution I went with the least number of possible actors to just solve the problem at hand.
That means we deal only with a single class `CheckOut` and a datastructure representing all rules for pricing.

I have reached into my functional toolbox and decided to apply the `reduce` pattern to slowly reduce the problem until there is no work left anymore.
The rules definition is enumerable and is ordered by descending priority.
This allows us to apply them all in sequence until they are not applicable anymore.
We re-calculate the totals from the start at any shopping cart change.
This looks a bit wasteful (we discard the current state and start from scratch on any change) but those operations
would hardly become a problem for most practical requirements with current hardware - we do basic math with in-memory data. This is ridiculously cheap.

Pros:

  - just enough code to solve the problem
  - functional algorithm allows us to trace every step of the calculation (uncomment log statements + run tests)
  - it is quite easy to understand

Cons:

  - it is tightly coupled to the data representation of the pricing rules
  - the algorithm is inflexible because it considers only combinations of "Item Name" X "Amount"
  - we should expect many other requirements from the business side, like: bundles, pricing based on current checkout time, pricing based on user demographic attributes / profile / history /etc...
  - the totals calculation happens on "writes", not on "reads", this might be not desired from the business perspective
