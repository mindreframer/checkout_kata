module CheckoutKata
  module V1
    class CheckOut
      attr_accessor :rules
      def initialize(rules)
        @rules = rules
        @total = 0
      end

      def scan(item)
        items << item
        increment(item)
        apply_rules
      end

      def total
        @total
      end

      private

      def items
        @items ||=[]
      end

      def items_map
        @items_map ||={}
      end

      def increment(item, amount=1)
        items_map[item] ||= 0
        items_map[item] += 1
        items_map
      end

      def apply_rules
        log "\nSTART: items_map: #{items_map.inspect}, #{items.inspect}"
        items_map_copy = @items_map.dup
        new_total      = 0

        rules.each do |rule|
          name, count, price = rule # destructure

          # apply until not applicable anymore
          while items_map_copy[name] && items_map_copy[name] >= count
            new_total += price
            items_map_copy[name] -= count
            log "** applied #{rule.inspect}"
            log items_map_copy.inspect
          end
        end
        log "TOTAL: #{new_total}"
        @total = new_total
      end

      def log(str)
        # puts str
      end
    end
  end
end
