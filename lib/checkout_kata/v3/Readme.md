# V3 (extract cart as separate class) solution for the checkout kata (http://codekata.com/kata/kata09-back-to-the-checkout/)

We moved some code from `CheckOut` class to `Cart` class and removed logging code.
This streamlines logic quite a bit and make the calculation for totals more readable.

Pros:

  - increased readability

Cons:

  - performance implications from V2 still apply
