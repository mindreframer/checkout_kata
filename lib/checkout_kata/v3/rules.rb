module CheckoutKata
  module V3
    class AbstractRule
      # applies this rule to the cart state
      #
      # @param current_cart_state [Map [Item -> amount] current_cart_state
      #
      # @return [Integer]              calculated amount for this rule
      # @return [Map [Item -> amount]] new current cart state
      #
      def apply(current_cart_state)
        raise "Implement in subclass"
      end

      # checks whether it is neccessary/possible to apply this rule
      #
      # @param current_cart_state [Map [Item -> amount] current_cart_state
      #
      # @return [boolean] should this rule be applied
      #
      def applicable?(current_cart_state)
        raise "Implement in subclass"
      end
    end

    # ItemAmountRule implements promotion rules based on specific amounts
    # of a single item in the shopping cart
    #
    class ItemAmountRule < CheckoutKata::V3::AbstractRule
      attr_accessor :name, :amount, :price
      def initialize(name:, amount:, price:)
        @name, @amount, @price = name, amount, price
      end

      def apply(current_cart_state)
        return 0, current_cart_state if not applicable?(current_cart_state)
        copy = current_cart_state.dup
        copy[name] -= amount
        return price, copy
      end

      def applicable?(current_cart_state)
        current_cart_state[name] && current_cart_state[name] >= amount
      end
    end
  end
end
