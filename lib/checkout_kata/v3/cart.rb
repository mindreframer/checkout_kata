module CheckoutKata
  module V3
    class Cart
      def add(item)
        history << item
        increment(item)
      end

      def copy_items_map
        items_map.dup
      end

      def history
        @history ||=[]
      end

      private

      def items_map
        @items_map ||={}
      end

      def increment(item, amount=1)
        items_map[item] ||= 0
        items_map[item] += amount
        items_map
      end
    end
  end
end
