# V2 (extract rules as separate interface) solution for the checkout kata (http://codekata.com/kata/kata09-back-to-the-checkout/)


We keep most of the logic from the V1 solution, but wrap rules into a class with two public methods:

  - `apply(current_cart_state) -> amount, new_current_cart_state`
  - `applicable?(current_cart_state) -> true/false`

This gives us flexibility to implement arbitrary pricing rules in future while conforming to that single interface.

Pros:

  - functional algorithm allows us to trace every step of the calculation (uncomment log statements + run tests)
  - we increase the flexibility for future requirements
  - totals calculation happens during querying

Cons:

  - quite some hash copy-ing needed to stay within pure functional paradigm (this is usually solved with immutable datastructures in other languages)
  - CheckOut class feels a bit bloated...
