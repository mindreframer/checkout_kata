module CheckoutKata
  module V2
    class CheckOut
      attr_accessor :rules
      def initialize(rules)
        @rules = rules
        @total = 0
      end

      def scan(item)
        items << item
        increment(item)
      end

      def total
        calculate_totals
      end

      private

      def items
        @items ||=[]
      end

      def items_map
        @items_map ||={}
      end

      def increment(item, amount=1)
        items_map[item] ||= 0
        items_map[item] += amount
        items_map
      end

      def calculate_totals
        log "\nSTART: items_map: #{items_map.inspect}, #{items.inspect}"
        current_state = @items_map.dup
        new_total     = 0

        rules.each do |rule|
          while rule.applicable?(current_state)
            amount, current_state = rule.apply(current_state)
            new_total += amount
            log "** applied #{rule.inspect}"
            log current_state.inspect
          end
        end
        log "TOTAL: #{new_total}"
        return new_total
      end

      def log(str)
        # puts str
      end
    end
  end
end
