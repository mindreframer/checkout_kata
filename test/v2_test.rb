require 'test_helper'

describe CheckoutKata::V2::CheckOut do
  rules = [
    ["A", 3,  130],
    ["B", 2,  45],
    ["A", 1,  50],
    ["B", 1,  30],
    ["C", 1,  20],
    ["D", 1,  15]
  ].map do |rule_data|
    CheckoutKata::V2::ItemAmountRule.new(
      name:   rule_data[0],
      amount: rule_data[1],
      price:  rule_data[2]
    )
  end

  TestHelper.validate_checkout(self, CheckoutKata::V2::CheckOut, rules)
end
