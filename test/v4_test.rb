require 'test_helper'

describe CheckoutKata::V4::CheckOut do
  rules = [
    ["A", 3,  130],
    ["B", 2,  45],
    ["A", 1,  50],
    ["B", 1,  30],
    ["C", 1,  20],
    ["D", 1,  15]
  ].map do |rule_data|
    CheckoutKata::V4::ItemAmountRule.new(
      name:   rule_data[0],
      amount: rule_data[1],
      price:  rule_data[2]
    )
  end

  TestHelper.validate_checkout(self, CheckoutKata::V4::CheckOut, rules)

  describe "specific rules" do
    rules = []
    rules << CheckoutKata::V4::ItemBundleRule.new({ name_amount_pairs: [["A", 2], ["B", 1]], price: 12 })
    rules << CheckoutKata::V4::ItemAmountRule.new({ name: "A", amount: 1, price: 5 })
    rules << CheckoutKata::V4::ItemAmountRule.new({ name: "B", amount: 1, price: 6 })

    it "applies ItemBundleRule" do
      checkout = CheckoutKata::V4::CheckOut.new(rules)
      checkout.scan("A")
      assert_equal checkout.total, 5
      checkout.scan("A")
      assert_equal checkout.total, 10
      checkout.scan("B")
      assert_equal checkout.total, 12
    end

    it do
      checkout = CheckoutKata::V4::CheckOut.new(rules)
      checkout.scan("B")
      assert_equal checkout.total, 6
      checkout.scan("A")
      assert_equal checkout.total, 11
      checkout.scan("A")
      assert_equal checkout.total, 12
    end
  end
end


describe CheckoutKata::V4::ItemBundleRule do

  def check_item_bundle_rule(params, state, price, new_state)
    rule = CheckoutKata::V4::ItemBundleRule.new(params)
    assert rule.applicable?(state)
    res_price, res_state = rule.apply(state)

    assert_equal price, res_price
    assert_equal res_state, new_state
  end

  describe "apply" do
    it do
      check_item_bundle_rule(
        { name_amount_pairs: [["A", 2], ["B", 1]], price: 12 },
        { "A" => 2, "B" => 2 },
        12,
        { "A" => 0, "B" => 1}
      )

      check_item_bundle_rule(
        { name_amount_pairs: [["A", 2], ["B", 1]], price: 13 },
        { "A" => 4, "B" => 3 },
        13,
        { "A" => 2, "B" => 2}
      )
    end
  end
end
