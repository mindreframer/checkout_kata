require 'test_helper'

describe CheckoutKata::V1::CheckOut do
  rules = [
    ["A", 3,  130],
    ["B", 2,  45],
    ["A", 1,  50],
    ["B", 1,  30],
    ["C", 1,  20],
    ["D", 1,  15]
  ]

  TestHelper.validate_checkout(self, CheckoutKata::V1::CheckOut, rules)
end
