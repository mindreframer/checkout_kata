$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'checkout_kata'

require 'minitest/autorun'

module TestHelper
  def self.validate_checkout(suite, subject, rules)
    suite.instance_eval do
      define_method :rules do
        rules
      end

      define_method :price do |goods|
        co = subject.new(rules)
        goods.split(//).each { |item| co.scan(item) }
        co.total
      end

      describe :totals do
        it do
          assert_equal(  0, price(""))
          assert_equal( 50, price("A"))
          assert_equal( 80, price("AB"))
          assert_equal(115, price("CDBA"))

          assert_equal(100, price("AA"))
          assert_equal(130, price("AAA"))
          assert_equal(180, price("AAAA"))
          assert_equal(230, price("AAAAA"))
          assert_equal(260, price("AAAAAA"))

          assert_equal(160, price("AAAB"))
          assert_equal(175, price("AAABB"))
          assert_equal(190, price("AAABBD"))
          assert_equal(190, price("DABABA"))
        end
      end

      describe :totals_incremental do
        it do
          co = subject.new(rules)
          assert_equal(  0, co.total)
          co.scan("A");  assert_equal( 50, co.total)
          co.scan("B");  assert_equal( 80, co.total)
          co.scan("A");  assert_equal(130, co.total)
          co.scan("A");  assert_equal(160, co.total)
          co.scan("B");  assert_equal(175, co.total)
        end
      end
    end
  end
end
